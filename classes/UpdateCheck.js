


if(typeof _CORE_UpdateCheck_Timer !== 'undefined') {
    clearInterval(_CORE_UpdateCheck_Timer);
}
let _CORE_UpdateCheck_Timer = setInterval(function() {
    let files = KnuddelsServer.getAppAccess().getOwnInstance().getRootInstance().updateAppFiles();
    files = files.filter(function(file) {
        return file.endsWith('.js');
    });
    files = files.filter(FileSystem.EXCLUDE_WWW);

    files.forEach(function(file) {
        FileSystem.execute(file);
    });
    if(files.length > 0) {
       KnuddelsServer.getDefaultLogger().debug('UPDATE: ' + JSON.stringify(files));
    }
},  KnuddelsServer.isTestSystem()?5000 : 60000);