execute('classes/ModulePersistence.js');

function Module() {
    this.visible = true;
    this._blockedModules = [];
    this.priority = 0;
    this.systemModule = false;
    this.isLoggingFunctions = false;

}
Module.prototype.stopHandle = function() {
    ModuleManager.stopHandle = true;
};


Module.prototype.register = function() {
    ModuleManager.self._modules.push(this);
};





/**
 *
 * @returns {ModulePersistence}
 */
Module.prototype.getPersistence = function() {
    if(typeof this._persistence === "undefined") {
        /**
         *
         * @type {ModulePersistence}
         * @private
         */
        this._persistence = new ModulePersistence(this);
    }
    return this._persistence;
};

Module.prototype.isActivated = function isActivated() {
    return this.getPersistence().getNumber('activated',0)===1;
};

/**
 *
 * @param {User} user
 * @returns {boolean}
 */
Module.prototype.deactivate = function deactivate(user) {
    this.getPersistence().setNumber('activated', 0);
    return true;
};

/**
 *
 * @param {User} user
 * @returns {boolean}
 */
Module.prototype.activate = function activate(user) {
    var blocked = this._blockedModules;
    for(var i = 0; i < blocked.length; i++) {
        var mod = ModuleManager.self.findModule(blocked[i]);
        if(mod != null && mod.isActivated()) {
            user._sendPrivateMessage('_$THISMOD_ konnte nicht aktiviert werden, da ein Konflikt mit _$OTHERMOD_ existiert.'.formater({
                THISMOD: this.toString().escapeKCode(),
                OTHERMOD: mod.toString().escapeKCode()
            }));
            return false;
        }
    }


    this.getPersistence().setNumber('activated', 1);
    return true;
};

Module.prototype.toString = function toString() {
    return this.constructor.name;
};


/**
 * Gibt die Sichtbarkeit des Moduls zurück.
 * @return {boolean}
 */
Module.prototype.isVisible = function isVisible() {
    if(typeof this.visible === "undefined")
        return true;

    return this.visible;
};

Module.prototype.getModuleDescription = null;

Module.prototype.getModuleHelp = null;