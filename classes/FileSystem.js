let FileSystem = {};

/**
 * Bindet die Datei im globalen Kontext ein. Die Datei wird geflagged und kann nicht erneut durch require eingebunden werden.
 * @param {string} file
 * @returns undefined
 */
FileSystem.require = function (file) {
    return KnuddelsServer.require(file);
};

/**
 * Führt die angegebene Datei im globalen Kontext aus.
 * @param {string} file
 * @returns undefined
 */
FileSystem.execute = function (file) {
    return KnuddelsServer.execute(file);
};

/**
 * Updated alle Dateien der App und gibt eine Liste der geänderten Dateien zurück.
 * @returns {String[]}
 */
FileSystem.updateAppFiles = function () {
    return KnuddelsServer.getAppAccess().getOwnInstance().getRootInstance().updateAppFiles();
};


/**
 *
 * @param {String} path
 * @param {boolean} [recursive=false]
 * @param {function[]} [filter=[]]
 * @returns {String[]}
 */
FileSystem.listFiles = function (path, recursive, filter) {
    recursive = typeof recursive === 'undefined' ? false : recursive;
    filter = typeof filter === 'undefined' ? [] : filter;

    let _files = KnuddelsServer.listFiles(path);
    _files.forEach(function (file) {
        if (recursive && file.endsWith('/')) {
            _files = _files.concat(FileSystem.listFiles(file, true));
        }
    });

    filter.forEach(function (func) {
        _files = _files.filter(func);
    });

    return _files;
};

/**
 * Filtert alle nicht .js Dateien heraus
 * @param a
 * @returns {boolean}
 */
FileSystem.ONLY_JS = function (a) {
    return a.endsWith('.js');
};
/**
 * Filtert, so dass nur noch Ordner returned werden
 * @param a
 * @returns {boolean}
 */
FileSystem.ONLY_DIR = function (a) {
    return a.endsWith('/');
};
/**
 * Filtert alle Ordner raus
 * @param a
 * @return {boolean}
 */
FileSystem.NO_DIR = function (a) {
    return !a.endsWith('/');
};
/**
 * filtert den www Ordner
 * @param a
 * @returns {boolean}
 */
FileSystem.EXCLUDE_WWW = function (a) {
    return !a.endsWith('www/');
};

/**
 * Bindet ein ganzes Verzeichnis per require ein.
 * @param {string} path
 * @param {boolean} [recursive=false]
 * @param {function[]} [filter=[]]
 */
FileSystem.requireDir = function (path, recursive, filter) {
    recursive = typeof recursive === 'undefined' ? false : recursive;
    filter = typeof filter === 'undefined' ? 0 : filter;
    let files = FileSystem.listFiles(path, recursive, [FileSystem.ONLY_JS].concat(filter));
    files.forEach(function (file) {
        FileSystem.require(file);
    });
};

/**
 * Bindet ein ganzes Verzeichnis per execute ein.
 * @param {string} path
 * @param {boolean} [recursive=false]
 * @param {function[]} [filter=[]]
 */
FileSystem.executeDir = function (path, recursive, filter) {
    recursive = typeof recursive === 'undefined' ? false : recursive;
    filter = typeof filter === 'undefined' ? [] : filter;
    let files = FileSystem.listFiles(path, recursive, [FileSystem.ONLY_JS].concat(filter));
    files.forEach(function (file) {
        FileSystem.execute(file);
    });
};


/**
 * Gibt den Datei/Ordnernamen eines Pfades zurück. /includes/init.js returned init.js
 * @param {string} path
 * @returns {string}
 */
FileSystem.basename = function (path) {
    let tmp = path.endsWith('/') ? path.substr(0, path.length - 1) : path;
    let base = tmp.replace(/.*\//, "");

    return path.endsWith('/') ? base + '/' : base;

};

/**
 * Gibt die Ordnerstruktur der Datei zurück
 * @param {string} path
 * @returns {string}
 */
FileSystem.dirname = function (path) {
    let parts = path.split('/');
    let pathCombine = [];

    parts.forEach(function (part) {
        if (part === '') {
            return;
        }
        if (part === '.') {
            return;
        }
        if (part === '..') {
            pathCombine.pop();
            return;
        }
        pathCombine.push(part);
    });


    if (pathCombine.length === 1) {
        return pathCombine[0];
    }
    pathCombine.pop();

    return pathCombine.join('/') + '/';

};

/**
 * Prüft ob eine Datei/Ordner existiert
 * @param {string} path
 * @returns {boolean}
 */
FileSystem.exists = function (path) {
    let filename = FileSystem.basename(path);
    let dir = FileSystem.dirname(path);
    let files = FileSystem.listFiles(dir);
    return files.indexOf(dir + filename) >= 0;

};
 


