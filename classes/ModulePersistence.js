/**
 *
 * @param {Module} module
 * @constructor
 */
function ModulePersistence(module) {
    /**
     *
     * @type {Module}
     */
    this.module = module;
}


/**
 *
 * @returns {String}
 */
ModulePersistence.prototype._getKey = function(longKey) {
    if(typeof ModulePersistence.keyMap[longKey] === 'undefined') {
        let ind = ModulePersistence.keyMap.ind;
        let shortKey = '_'+ind;
        KnuddelsServer.getPersistence().setString('_'+shortKey,longKey);
        ModulePersistence.keyMap.ind++;
        ModulePersistence.keyMap[longKey] = shortKey;
    }


    return ModulePersistence.keyMap[longKey];
};

/**
 *
 * @returns {String}
 */
ModulePersistence.prototype.getPrefix = function getPrefix() {
    return "m" + this.module.constructor.name + "_";
};


/**
 *
 * @param {String} str
 * @returns {String}
 */
ModulePersistence.prototype.getKey = function getKey(str) {
    let longKey = this.getLongKey(str);
    return this._getKey(longKey);
};


/**
 *
 * @param {String} str
 * @returns {String}
 */
ModulePersistence.prototype.getLongKey = function getKey(str) {

    return this.getPrefix() + str;
};












/**
 *
 * @param {String} key
 * @returns {Boolean}
 */
ModulePersistence.prototype.hasString = function hasString (key) {

    let longKey = this.getLongKey(key);
    let shortKey = this.getKey(key);
    if(KnuddelsServer.getPersistence().hasString(longKey)) {
        let oldValue = KnuddelsServer.getPersistence().getString(longKey);
        KnuddelsServer.getPersistence().setString(shortKey, oldValue);
        KnuddelsServer.getPersistence().deleteString(longKey)
    }

    return KnuddelsServer.getPersistence().hasString(shortKey);
};


/**
 *
 * @param {String} key
 * @param {String} defaultValue
 * @returns {String}
 */
ModulePersistence.prototype.getString = function getString (key, defaultValue) {

    let longKey = this.getLongKey(key);
    let shortKey = this.getKey(key);
    if(KnuddelsServer.getPersistence().hasString(longKey)) {
        let oldValue = KnuddelsServer.getPersistence().getString(longKey);
        KnuddelsServer.getPersistence().setString(shortKey, oldValue);
        KnuddelsServer.getPersistence().deleteString(longKey)
    }
    return KnuddelsServer.getPersistence().getString(shortKey, defaultValue);
};

/**
 *
 * @param {String} key
 * @param {String} value
 */
ModulePersistence.prototype.setString = function setString(key, value) {
    let longKey = this.getLongKey(key);
    let shortKey = this.getKey(key);
    if(KnuddelsServer.getPersistence().hasString(longKey)) {
        let oldValue = KnuddelsServer.getPersistence().getString(longKey);
        KnuddelsServer.getPersistence().setString(shortKey, oldValue);
        KnuddelsServer.getPersistence().deleteString(longKey)
    }
    KnuddelsServer.getPersistence().setString(shortKey, value);
};


/**
 *
 * @param {String} key
 */
ModulePersistence.prototype.deleteString = function deleteString(key) {
    let longKey = this.getLongKey(key);
    let shortKey = this.getKey(key);
    if(KnuddelsServer.getPersistence().hasString(longKey)) {
        let oldValue = KnuddelsServer.getPersistence().getString(longKey);
        KnuddelsServer.getPersistence().setString(shortKey, oldValue);
        KnuddelsServer.getPersistence().deleteString(longKey)
    }
    KnuddelsServer.getPersistence().deleteString(shortKey);
};





/**
 *
 * @param {String} key
 * @returns {Boolean}
 */
ModulePersistence.prototype.hasObject = function hasObject(key) {
    let longKey = this.getLongKey(key);
    let shortKey = this.getKey(key);
    if(KnuddelsServer.getPersistence().hasObject(longKey)) {
        let oldValue = KnuddelsServer.getPersistence().getObject(longKey);
        KnuddelsServer.getPersistence().setObject(shortKey, oldValue);
        KnuddelsServer.getPersistence().deleteObject(longKey)
    }
    return KnuddelsServer.getPersistence().hasObject(shortKey);
};


/**
 *
 * @param {String} key
 * @param {Object} defaultValue
 * @returns {Object}
 */
ModulePersistence.prototype.getObject = function getObject(key, defaultValue) {
    let longKey = this.getLongKey(key);
    let shortKey = this.getKey(key);
    if(KnuddelsServer.getPersistence().hasObject(longKey)) {
        let oldValue = KnuddelsServer.getPersistence().getObject(longKey);
        KnuddelsServer.getPersistence().setObject(shortKey, oldValue);
        KnuddelsServer.getPersistence().deleteObject(longKey)
    }
    return KnuddelsServer.getPersistence().getObject(shortKey, defaultValue);
};

/**
 *
 * @param {String} key
 * @param {Object} value
 */
ModulePersistence.prototype.setObject = function setObject(key, value) {
    let longKey = this.getLongKey(key);
    let shortKey = this.getKey(key);
    if(KnuddelsServer.getPersistence().hasObject(longKey)) {
        let oldValue = KnuddelsServer.getPersistence().getObject(longKey);
        KnuddelsServer.getPersistence().setObject(shortKey, oldValue);
        KnuddelsServer.getPersistence().deleteObject(longKey)
    }
    KnuddelsServer.getPersistence().setObject(shortKey, value);
};


/**
 *
 * @param {String} key
 */
ModulePersistence.prototype.deleteObject = function deleteObject(key) {
    let longKey = this.getLongKey(key);
    let shortKey = this.getKey(key);
    if(KnuddelsServer.getPersistence().hasObject(longKey)) {
        let oldValue = KnuddelsServer.getPersistence().getObject(longKey);
        KnuddelsServer.getPersistence().setObject(shortKey, oldValue);
        KnuddelsServer.getPersistence().deleteObject(longKey)
    }
    KnuddelsServer.getPersistence().deleteObject(this.getKey(key));
};



/**
 *
 * @param {String} key
 * @returns {Boolean}
 */
ModulePersistence.prototype.hasNumber = function hasNumber(key) {
    let longKey = this.getLongKey(key);
    let shortKey = this.getKey(key);
    if(KnuddelsServer.getPersistence().hasNumber(longKey)) {
        let oldValue = KnuddelsServer.getPersistence().getNumber(longKey);
        KnuddelsServer.getPersistence().setNumber(shortKey, oldValue);
        KnuddelsServer.getPersistence().deleteNumber(longKey)
    }
    return KnuddelsServer.getPersistence().hasNumber(shortKey);
};


/**
 *
 * @param {String} key
 * @param {Number} defaultValue
 * @returns {Number}
 */
ModulePersistence.prototype.getNumber = function getNumber(key, defaultValue) {
    if(typeof defaultValue === 'undefined') {
        defaultValue = 0;
    }
    let longKey = this.getLongKey(key);
    let shortKey = this.getKey(key);
    if(KnuddelsServer.getPersistence().hasNumber(longKey)) {
        let oldValue = KnuddelsServer.getPersistence().getNumber(longKey);
        KnuddelsServer.getPersistence().setNumber(shortKey, oldValue);
        KnuddelsServer.getPersistence().deleteNumber(longKey)
    }
    return KnuddelsServer.getPersistence().getNumber(shortKey, defaultValue);
};

/**
 *
 * @param {String} key
 * @param {Number} value
 */
ModulePersistence.prototype.setNumber = function setNumber(key, value) {
    let longKey = this.getLongKey(key);
    let shortKey = this.getKey(key);
    if(KnuddelsServer.getPersistence().hasNumber(longKey)) {
        let oldValue = KnuddelsServer.getPersistence().getNumber(longKey);
        KnuddelsServer.getPersistence().setNumber(shortKey, oldValue);
        KnuddelsServer.getPersistence().deleteNumber(longKey)
    }
    KnuddelsServer.getPersistence().setNumber(shortKey, value);
};



/**
 *
 * @param {String} key
 * @param {Number} value
 * @returns {Number}
 */
ModulePersistence.prototype.addNumber = function addNumber(key, value) {
    let longKey = this.getLongKey(key);
    let shortKey = this.getKey(key);
    if(KnuddelsServer.getPersistence().hasNumber(longKey)) {
        let oldValue = KnuddelsServer.getPersistence().getNumber(longKey);
        KnuddelsServer.getPersistence().setNumber(shortKey, oldValue);
        KnuddelsServer.getPersistence().deleteNumber(longKey)
    }
    return KnuddelsServer.getPersistence().addNumber(shortKey, value);
};

/**
 *
 * @param {String} key
 */
ModulePersistence.prototype.deleteNumber = function deleteNumber(key) {

    let shortKey = this.getKey(key);

    KnuddelsServer.getPersistence().deleteNumber(shortKey);
};





ModulePersistence.prototype.updateUserNumberKey = function updateUserNumberKey(oldkey, newkey, forceDelete) {

    let oshortKey = this.getKey(oldkey);
    let nshortKey = this.getKey(newkey);



    forceDelete = forceDelete || false;
    if(forceDelete) {
        UserPersistenceNumbers.deleteAll(this.getKey(nshortKey));
    }
    UserPersistenceNumbers.updateKey(oshortKey, nshortKey);
};

/**
 *
 * @param {User} user
 * @param {String} key
 * @param {Number} defaultValue
 * @returns {Number}
 */
ModulePersistence.prototype.getUserNumber = function getUserNumber(user, key, defaultValue) {
    if(typeof defaultValue === 'undefined') {
        defaultValue = 0;
    }

    let shortKey = this.getKey(key);

    return user.getPersistence().getNumber(shortKey, defaultValue);
};

/**
 *
 * @param {User} user
 * @param {String} key
 * @returns {Boolean}
 */
ModulePersistence.prototype.hasUserNumber = function hasUserNumber(user, key) {

    let shortKey = this.getKey(key);

    return user.getPersistence().hasNumber(shortKey);
};

/**
 *
 * @param {User} user
 * @param {String} key
 * @param {Number} value
 */
ModulePersistence.prototype.setUserNumber = function setUserNumber(user, key, value) {

    let shortKey = this.getKey(key);

    user.getPersistence().setNumber(shortKey, value);
};

/**
 *
 * @param {User} user
 * @param {String} key
 * @param {Number} value
 * @returns {Number}
 */
ModulePersistence.prototype.addUserNumber = function addUserNumber(user, key, value) {

    let shortKey = this.getKey(key);

    return user.getPersistence().addNumber(shortKey, value);
};



/**
 *
 * @param {User} user
 * @param {String} key
 */
ModulePersistence.prototype.deleteUserNumber = function deleteUserNumber(user, key) {

    let shortKey = this.getKey(key);

    user.getPersistence().deleteNumber(shortKey);
};

/**
 *
 * @param {User} user
 * @param {String} key
 * @param {Object} object
 */
ModulePersistence.prototype.setUserObject = function setUserObject(user, key, object) {
    let longKey = this.getLongKey(key);
    let shortKey = this.getKey(key);
    if(user.getPersistence().hasObject(longKey)) {
        let oldValue = user.getPersistence().getObject(longKey);
        user.getPersistence().setObject(shortKey, oldValue);
        user.getPersistence().deleteObject(longKey);
    }
    user.getPersistence().setObject(shortKey, object);
};

/**
 *
 * @param {User} user
 * @param {String} key
 * @param {Object} defaultValue
 * @returns {Object}
 */
ModulePersistence.prototype.getUserObject = function getUserObject(user, key, defaultValue) {
    let longKey = this.getLongKey(key);
    let shortKey = this.getKey(key);
    if(user.getPersistence().hasObject(longKey)) {
        let oldValue = user.getPersistence().getObject(longKey);
        user.getPersistence().setObject(shortKey, oldValue);
        user.getPersistence().deleteObject(longKey);
    }

    if(typeof defaultValue === 'undefined')
        return user.getPersistence().getObject(shortKey);

    return user.getPersistence().getObject(shortKey, defaultValue);
};


/**
 *
 * @param {User} user
 * @param {String} key
 * @returns {Boolean}
 */
ModulePersistence.prototype.hasUserObject = function hasUserNumber(user, key) {
    let longKey = this.getLongKey(key);
    let shortKey = this.getKey(key);
    if(user.getPersistence().hasObject(longKey)) {
        let oldValue = user.getPersistence().getObject(longKey);
        user.getPersistence().setObject(shortKey, oldValue);
        user.getPersistence().deleteObject(longKey);
    }
    return user.getPersistence().hasObject(shortKey);
};



/**
 *
 * @param {User} user
 * @param {String} key
 */
ModulePersistence.prototype.deleteUserObject = function deleteUserObject(user, key) {
    let longKey = this.getLongKey(key);
    let shortKey = this.getKey(key);
    if(user.getPersistence().hasObject(longKey)) {
        let oldValue = user.getPersistence().getObject(longKey);
        user.getPersistence().setObject(shortKey, oldValue);
        user.getPersistence().deleteObject(longKey);
    }
    user.getPersistence().deleteObject(shortKey);
};

ModulePersistence.prototype.hasUserString = function hasUserString(user, key) {
    let longKey = this.getLongKey(key);
    let shortKey = this.getKey(key);
    if(user.getPersistence().hasString(longKey)) {
        let oldValue = user.getPersistence().getString(longKey);
        user.getPersistence().getString(shortKey, oldValue);
        user.getPersistence().deleteString(longKey);
    }
    return user.getPersistence().hasString(shortKey);
};

ModulePersistence.prototype.getUserString = function getUserString(user, key, defaultValue) {
    if(typeof defaultValue === 'undefined') {
        defaultValue = '';
    }
    let longKey = this.getLongKey(key);
    let shortKey = this.getKey(key);
    if(user.getPersistence().hasString(longKey)) {
        let oldValue = user.getPersistence().getString(longKey);
        user.getPersistence().getString(shortKey, oldValue);
        user.getPersistence().deleteString(longKey);
    }

    return user.getPersistence().getString(shortKey, defaultValue);
};

ModulePersistence.prototype.setUserString = function getUserString(user, key, value) {
    let longKey = this.getLongKey(key);
    let shortKey = this.getKey(key);
    if(user.getPersistence().hasString(longKey)) {
        let oldValue = user.getPersistence().getString(longKey);
        user.getPersistence().getString(shortKey, oldValue);
        user.getPersistence().deleteString(longKey);
    }
    return user.getPersistence().setString(shortKey, value);
};

ModulePersistence.prototype.deleteUserString = function deleteUserString(user, key) {
    let longKey = this.getLongKey(key);
    let shortKey = this.getKey(key);
    if(user.getPersistence().hasString(longKey)) {
        let oldValue = user.getPersistence().getString(longKey);
        user.getPersistence().getString(shortKey, oldValue);
        user.getPersistence().deleteString(longKey);
    }
    user.getPersistence().deleteString(shortKey);
};


/**
 *
 * @returns {{}}
 */
ModulePersistence.prototype.getKeyMap = function() {
    let keyMap = {};
    let prefix = this.getPrefix();
    for(let key in ModulePersistence.keyMap) {
        if(key.startsWith(prefix)) {
            keyMap[key] = ModulePersistence.keyMap[key];
        }
    }

    return keyMap;
};

/**
 * Löscht die gesamte Persistenz vom Modul
 */
ModulePersistence.prototype.deleteAll = function() {
    let keyMap = this.getKeyMap();
    for(let key in keyMap) {
        let shortKey = keyMap[key];
        UserPersistenceNumbers.deleteAll(shortKey);
        UserPersistenceObjects.deleteAll(shortKey);
        UserPersistenceStrings.deleteAll(shortKey);
        KnuddelsServer.getPersistence().deleteString(shortKey);
        KnuddelsServer.getPersistence().deleteObject(shortKey);
        KnuddelsServer.getPersistence().deleteNumber(shortKey);
    }
};









ModulePersistence.keyMap = {
    ind: 0
};
while(true) {
    let ind = ModulePersistence.keyMap.ind;
    let shortKey = '_'+ind;
    let longKey =KnuddelsServer.getPersistence().getString('_'+shortKey,null);
    if(longKey == null) {
        break;
    }
    ModulePersistence.keyMap[longKey] = shortKey;
    ModulePersistence.keyMap.ind++;
}


//rename UserPersistenceNumbers
let allKeys = UserPersistenceNumbers.getAllKeys();
allKeys.forEach(function(key) {
    let regex = /^m([A-Z].*)_/g;
    if(key.match(regex)) {
        let longKey = key;
        let shortKey = '';
        if(typeof ModulePersistence.keyMap[longKey] === 'undefined') {
            let ind = ModulePersistence.keyMap.ind;
            shortKey = '_'+ind;
            KnuddelsServer.getPersistence().setString('_'+shortKey,longKey);
            ModulePersistence.keyMap.ind++;
            ModulePersistence.keyMap[longKey] = shortKey;
        }
        shortKey = ModulePersistence.keyMap[longKey];
        try { UserPersistenceNumbers.updateKey(longKey, shortKey); } catch(ex) { }
        UserPersistenceNumbers.deleteAll(longKey);
    }

});
