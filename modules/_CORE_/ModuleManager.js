/**
 * @extends Module
 * @constructor
 */
function ModuleManager() {
    this.systemModule = true;
    /**
     *
     * @type {Module[]}
     */
    this._modules = [this];
    this._commands = [];

    this._moduleHooks = {};

    this.visible = false;

    this.loadModules();

    setInterval(this.masterTimerHandler.bind(this), 1000);

    this.UI = AppContent.popupContent(new HTMLFile('_CORE_/ModuleManager/index.html'), 850, 550);
    this.BAR = AppContent.headerbarContent(new HTMLFile('_CORE_/ModuleManager/bar.html'), 50);
    this.MEASUREMENT = {};
    this._SHOWMEASURE = false;
}

ModuleManager.stopHandle = false;

Object.extends(ModuleManager, Module);

/**
 *
 * @returns {Module[]}
 */
ModuleManager.prototype.getModules = function getModules() {
    return this._modules;
};

/**
 *
 * @returns {boolean}
 */
ModuleManager.prototype.isActivated = function() {
    return true;
};



/**
 *
 * @param name
 * @returns {Module}
 */
ModuleManager.prototype.findModule = function findModule(name) {
    try {
        return eval(name + ".self")
    } catch(ex) {
        return null;
    }

};

ModuleManager.prototype.whatIsCm = function whatIsCM(user) {
    user._sendPrivateMessage('°RR°_Dieser Channel nutzt die ChannelMaster App°°_°#°' +
        'Um diese App auch in deinem Channel zu installieren gebe °BB°_/apps install 30559674.ChannelMaster_°° in deinem MyChannel ein.');
};

ModuleManager.prototype.getModuleDataForUI = function(user) {
    let ret = [];


    for(var i = 0; i < this._modules.length; i++) {
        let module = this._modules[i];
        if(module.isVisible() || user._isAppDeveloper())
            ret.push({
                moduleName: module.toString(),
                activated: module.isActivated(),
                hasUI: typeof module.openDefaultUI !== "undefined"
            });
    }

    /*{
     moduleName: 'Modul',
     activated: true
     },*/

    return ret;
};

ModuleManager.prototype.sendAdminOverviewUI = function sendAdminOverviewUI(user) {
    if(!user.canSendAppContent(this.UI)) {
        user._sendPrivateMessage('Du kannst dieses UI nicht empfangen.');
        return;
    }
    let session = user.sendAppContent(this.UI);
    session.sendEvent('moduleDatas', this.getModuleDataForUI(user));
};

ModuleManager.prototype.updateUI = function() {
    this.UI.getSessions().forEach(function(session) {
        session.sendEvent('moduleDatas', this.getModuleDataForUI(session.getUser()));
    },this);
};

ModuleManager.prototype.cmdModuleManager = function(user, params, func) {
    if(params.trim().toLowerCase() === 'whatiscm') {
        this.whatIsCm(user);
        return;
    }

    if(!(user._isAppManager() || user._isAppDeveloper())) {
        return;
    }





    let action ="";
    let ind = params.indexOf(':');
    if(ind < 0) {
        action = params.toLowerCase();
        params = "";
    } else {
        action = params.substr(0, ind).toLocaleLowerCase();
        params = params.substr(ind+1);
    }


    if(action === 'ui') {
        let module = this.findModule(params);
        if(module == null || (!module.isVisible() && !user._isAppDeveloper())) {
            user._sendPrivateMessage('Das Modul ' + params + ' wurde nicht gefunden.');
            return;
        }
        if(!module.isActivated()) {
            user._sendPrivateMessage('Das Modul ist derzeit deaktiviert.');
            return;
        }

        if(typeof module.openDefaultUI === 'function') {
            module.openDefaultUI(user);
        } else {
            user._sendPrivateMessage('Dieses Modul hat keine UI.');
        }
        return;
    }

    if(action === 'help') {
        let module = this.findModule(params);
        if(module == null || (!module.isVisible() && !user._isAppDeveloper())) {
            user._sendPrivateMessage('Das Modul ' + params + ' wurde nicht gefunden.');
            return;
        }
        if(typeof module.getModuleDescription === 'function') {
            user._sendPrivateMessage(module.getModuleDescription(user));
        } else {
            user._sendPrivateMessage('Dieses Modul hat keine Beschreibung.');
        }

        if(typeof module.getModuleHelp === 'function') {
            user._sendPrivateMessage(module.getModuleHelp(user));
        } else {
            user._sendPrivateMessage('Dieses Modul hat keine Hilfe.');
        }
        return;
    }

    if(action === 'activatemodule') {
        let module = this.findModule(params);
        if(module == null || (!module.isVisible() && !user._isAppDeveloper())) {
            user._sendPrivateMessage('Das Modul ' + params + ' wurde nicht gefunden.');
            return;
        }
        if(module.isActivated()) {
            user._sendPrivateMessage('Dieses Modul ist bereits aktiviert.');
            return;
        }
        let activated = module.activate(user);
        if(activated) {
            this.refreshHooks(true);
            this.updateUI();
            if(!App.owner.equals(user)) {
                App.owner.sendPostMessage('°BB°Modul aktiviert°°', '°BB°_$USER_°° hat das Modul °BB°_$MODULE_°° aktiviert.'.formater({
                    MODULE: module.toString().escapeKCode(),
                    USER: user.getProfileLink()
                }));
            }
            if(typeof module.onActivated !== "undefined") {
                module.onActivated();
            }
        } else {
            user._sendPrivateMessage('Dieses Modul konnte nicht aktiviert werden.');
        }

        return;
    }

    if(action === 'deactivatemodule') {
        let module = this.findModule(params);
        if(module == null || (!module.isVisible() && !user._isAppDeveloper())) {
            user._sendPrivateMessage('Das Modul ' + params + ' wurde nicht gefunden.');
            return;
        }
        if(!module.isActivated()) {
            user._sendPrivateMessage('Dieses Modul ist bereits deaktiviert.');
            return;
        }
        let activated = module.deactivate(user);
        if(activated) {
            this.refreshHooks(true);
            this.updateUI();
            if(!App.owner.equals(user)) {
                App.owner.sendPostMessage('°RR°Modul deaktiviert°°', '°BB°_$USER_°° hat das Modul °BB°_$MODULE_°° deaktiviert.'.formater({
                    MODULE: module.toString().escapeKCode(),
                    USER: user.getProfileLink()
                }));
            }
            if(typeof module.onDeactivated !== "undefined") {
                module.onDeactivated();
            }
        } else {
            user._sendPrivateMessage('Dieses Modul konnte nicht deaktiviert werden.');
        }


    }
};

/**
 *
 * @param user
 */
ModuleManager.prototype.cmdAv = function Av(user){
    if(user._isAppManager()) {
        this.sendAdminOverview(user);
    }
};


ModuleManager.prototype.handleFunction = function() {

    let _self = ModuleManager.self;
    App._LASTUSER = arguments[0];
    App._LASTCMD  = arguments[2];
    let m = _self._commands[arguments[2].toLowerCase()];
    let cmd         = m.cmd;
    let module      = m.m;
    if(module.isLoggingFunctions && !arguments[0]._isAppDeveloper()) {
        KnuddelsServer.getDefaultLogger().warn(arguments[0] + ' nutzt /' + arguments[2] + ' ' + arguments[1]);
    }

    module[cmd].apply(module, arguments);
    App._LASTCMD = '$CMD';

};

ModuleManager.prototype.refreshHooks = function refreshHooks(allChannel) {
    if(allChannel) {

        /**
         *
         * @type {AppInstance[]}
         */
        let instances = KnuddelsServer.getAppAccess().getOwnInstance().getAllInstances(true);
        instances.forEach(function(instance) {
            instance.sendAppEvent('ModuleManager', 'refreshHooks');
        });
        return;
    }


    this._modules.sort(function(a,b) {
        if(a.systemModule && !b.systemModule) {
            return 1;
        }
        return a.priority - b.priority;
    });

    let moduleHooks = {};
    let commands = {};
    let reservedCommands = KnuddelsServer.getChannel()._getRegisteredChatCommandNames();

    for(let i = 0; i < this._modules.length; i++) {
        let module = this._modules[i];
        if(!module.isActivated()) {
            continue;
        }

        for(let key in module) {
            if(key.startsWith('on') || key.startsWith('may')) {
                if(typeof moduleHooks[key] === "undefined") {
                    moduleHooks[key] = [];
                }
                moduleHooks[key].push(module);
            } else if(key.startsWith('cmd')) {
                let cmd = key.substring(3).toLowerCase();
                if(typeof reservedCommands[cmd] !== 'undefined') {
                    let appInfoOfOtherApp = reservedCommands[cmd].getAppInfo();
                    let oldCmd = cmd;
                    cmd = 'cmv2'+cmd;
                    if(App.owner.isOnlineInChannel()) {
                        App.owner.sendPrivateMessage("Command /" + oldCmd + " already registered by " + appInfoOfOtherApp.getAppName() + '(DEV: ' + appInfoOfOtherApp.getAppDeveloper() +') --- renaming Command to /' + cmd)
                    } else {
                        App.owner.sendPostMessage("/-Command Konflikt", "Command /" + oldCmd + " already registered by " + appInfoOfOtherApp.getAppName() + '(DEV: ' + appInfoOfOtherApp.getAppDeveloper() +') --- renaming Command to /' + cmd)
                    }
                    KnuddelsServer.getDefaultLogger().error("Command /" + oldCmd + " already registered by " + appInfoOfOtherApp.getAppName() + '(DEV: ' + appInfoOfOtherApp.getAppDeveloper() +') --- renaming Command to /' + cmd)
                }

                this._commands[cmd] = {m: module, cmd: key};
                commands[cmd] = this.handleFunction;
            }
        }
    }
    this._moduleHooks = moduleHooks;

    //delete app.hooks
    for(let key in App) {
        if(key.startsWith("on") || key.startsWith("may")) {
            delete App[key];
        }
    }

    function createHook(hookname) {
        return eval('App. '+hookname +' = function ' + hookname + '() { return ModuleManager.self.manageHook("' + hookname +'", arguments); };');
    }



    for(let key in this._moduleHooks) {
        createHook(key);
    }
    App.chatCommands = commands;
    this.broadcastAdminOverview();
    KnuddelsServer.refreshHooks();
};


/**
 *
 * @param {PrivateMessage} privateMessage
 */
ModuleManager.prototype.onPrivateMessage = function onPrivateMessage(privateMessage) {
    var user = privateMessage.getAuthor();
    var message = privateMessage.getText();



    if(user._isAppDeveloper()) {
        if(message.toLowerCase() === "restart") {
            KnuddelsServer.getAppAccess().getOwnInstance().getRootInstance().updateApp('', 'Restart');
            this.stopHandle();
        }
    }
};





/**
 *
 * @param {String} hookname
 * @param {Object[]} args
 * @returns {*}
 */
ModuleManager.prototype.manageHook = function manageHook(hookname, args) {
    args = Array.slice(args);

    ModuleManager.stopHandle = false;

    //simplecheck
    for(let i = 0; i < args.length; i++) {
        if(typeof args[i].javaClassName !== "undefined") {
            if(args[i].javaClassName === 'User') {
                App._LASTUSER = args[i];
                break;
            }
            let getUserFunctions = ['getAuthor','getUser'];
            for(let j = 0; j < getUserFunctions.length; j++) {
                let func = getUserFunctions[j];
                if (typeof args[i][func] !== "undefined") {
                    App._LASTUSER = args[i][func]();
                    break;
                }
            }
        }
    }


    if(hookname === 'onBeforeKnuddelReceived') {
        let transfer = args[0];
        if(typeof this._moduleHooks[hookname] !== "undefined"){
            for(let i = 0; i < this._moduleHooks[hookname].length; i++) {
                let module = this._moduleHooks[hookname][i];
                module[hookname].apply(module, args);
                if(transfer.isProcessed()) {
                    return;
                }
                if(ModuleManager.stopHandle) {
                    break;
                }
            }

        }
        transfer.accept();

    } else if(hookname === "mayShowPublicMessage" || hookname === "mayShowPublicActionMessage") {
        let allowed = true;
        if(typeof this._moduleHooks[hookname] !== "undefined"){
            for(let i = 0; i < this._moduleHooks[hookname].length; i++) {
                let module = this._moduleHooks[hookname][i];
                allowed &= module[hookname].apply(module, args);
                if(ModuleManager.stopHandle) {
                    break;
                }
            }
        }

        // noinspection JSValidateTypes
        return allowed == 1;
    } else if(hookname === 'mayJoinChannel') {
        if(typeof this._moduleHooks[hookname] !== "undefined"){
            for(let i = 0; i < this._moduleHooks[hookname].length; i++) {
                let module = this._moduleHooks[hookname][i];
                let ret = module[hookname].apply(module, args);
                if(typeof ret !== "undefined") {
                    return ret;
                }
                if(ModuleManager.stopHandle) {
                    break;
                }
            }
        }
        return ChannelJoinPermission.accepted();
    } else if(hookname.startsWith("on")) {

        if(typeof this._moduleHooks[hookname] !== "undefined"){
            for(var i = 0; i < this._moduleHooks[hookname].length; i++) {
                let module = this._moduleHooks[hookname][i];
                module[hookname].apply(module, args);
                if(ModuleManager.stopHandle) {
                    break;
                }
            }
        }
    }
};

ModuleManager.prototype.loadModules = function loadModules() {
    //load core
    let loadModules = [];
    loadModules = loadModules.concat(FileSystem.listFiles('modules/_CORE_/', true, [FileSystem.ONLY_JS, function(a) { return !a.endsWith('ModuleManager.js'); }]));
    loadModules.forEach(function(path) {
        FileSystem.execute(path);
        let name = FileSystem.basename(path);
        name = name.substr(0, name.length-3);
        if(typeof window[name] !== 'undefined') {
            window[name].self = new window[name];
        }
    });

};

ModuleManager.prototype.onEventReceived = function(user, type, data, session) {
    if(type === 'MM_activate') {
        this.stopHandle();
        this.cmdModuleManager(user, 'activateModule:'+data);  this.stopHandle();
    } else if(type === 'MM_deactivate') {
        this.stopHandle();
        this.cmdModuleManager(user, 'deactivateModule:'+data); this.stopHandle();
    } else if(type === 'MM_help') {
        this.stopHandle();
        this.cmdModuleManager(user, 'help:'+data);  this.stopHandle();
    } else if(type === 'MM_openUI') {
        this.stopHandle();
        var module = this.findModule(data.trim());
        if(module==null) {
            return;
        }
        if(!module.isActivated()) {
            return;
        }
        module.openDefaultUI(user);
    }
};


ModuleManager.prototype.masterTimerHandler = function() {



    let date = new Date();

    if(typeof this._lastDay === "undefined") {
        this._lastDay       = new Date(this.getPersistence().getNumber('_lastDay',      date.getTime()));
        this._lastHour      = new Date(this.getPersistence().getNumber('_lastHour',     date.getTime()));
        this._lastMinute    = new Date(this.getPersistence().getNumber('_lastMinute',   date.getTime()));
        this._lastWeek      = new Date(this.getPersistence().getNumber('_lastWeek',     date.getTime()));
        this._lastMonth     = new Date(this.getPersistence().getNumber('_lastMonth',    date.getTime()));
        this._lastYear      = new Date(this.getPersistence().getNumber('_lastYear',     date.getTime()));
    }
    if(date.getDate() !== this._lastDay.getDate()) {
        this.manageHook('onNewDay', [date, this._lastDay] );
        this._lastDay = date;
        this.getPersistence().setNumber('_lastDay', date.getTime());

    }

    if(date.getHours() !== this._lastHour.getHours()) {
        this.manageHook('onNewHour', [date, this._lastHour] );
        this._lastHour = date;
        this.getPersistence().setNumber('_lastHour', date.getTime());
    }

    if(date.getMinutes() !== this._lastMinute.getMinutes()) {
        this.manageHook('onNewMinute', [date, this._lastMinute] );
        this._lastMinute = date;
        this.getPersistence().setNumber('_lastMinute', date.getTime());
    }

    if(date.getWeek() !== this._lastWeek.getWeek()) {
        this.manageHook('onNewWeek', [date, this._lastWeek] );
        this._lastWeek = date;
        this.getPersistence().setNumber('_lastWeek', date.getTime());
    }

    if(date.getMonth() !== this._lastMonth.getMonth()) {
        this.manageHook('onNewMonth', [date, this._lastMonth] );
        this._lastMonth = date;
        this.getPersistence().setNumber('_lastMonth', date.getTime());
    }


    if(date.getYear() !== this._lastMonth.getYear()) {
        this.manageHook('onNewYear', [date, this._lastYear] );
        this._lastYear = date;
        this.getPersistence().setNumber('_lastYear', date.getTime());
    }





    for(let i = 0; i < this._modules.length; i++) {
        let module = this._modules[i];
        if(!module.isActivated()) {
            continue;
        }


        if(typeof module.timerHandler === 'function') {
            module.timerHandler(date);
        }
    }

};


/**
 *
 * @param user
 */
ModuleManager.prototype.onUserJoined = function onUserJoined(user) {
    if(user._isAppDeveloper() || user._isAppManager()) {
        this.sendUI(user);
    }
};

ModuleManager.prototype.onAppStart = function() {
    CHANNEL.getOnlineUsers(UserType.Human).forEach(function(user) {
        if(user._isAppDeveloper() || user._isAppManager()) {
            this.sendUI(user);
        }
    }, this);
};

ModuleManager.prototype.sendUI = function(user) {
    user.sendAppContent(this.BAR);
};

ModuleManager.prototype.onAppEventReceived = function onAppEventReceived(appInstance, type, data) {
    if(type === "ModuleManager") {
        if(data === "refreshHooks") {
            this.refreshHooks();
        }
    }
};

ModuleManager.prototype.broadcastAdminOverview = function broadcastAdminOverview() {
    let users = KnuddelsServer.getChannel().getOnlineUsers(UserType.Human);
    users.forEach(function(user) {
        if(user._isAppManager() || user._isAppDeveloper()) {
            this.sendAdminOverview(user);
        }
    }.bind(this));
};

ModuleManager.prototype.sendAdminOverview = function sendAdminOverview(user) {

};

ModuleManager.prototype.sendJoinOverview = function sendJoinOverview(user) {

};




/**
 * Verhindere das Deaktivieren des Moduls
 * @returns {boolean}
 */
ModuleManager.prototype.deactivate = function() {
    return false;
};

