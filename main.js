KnuddelsServer.execute('/classes/FileSystem.js');
FileSystem.executeDir('/libs/');
FileSystem.executeDir('/extensions/');
FileSystem.executeDir('/classes/');



var App = {};
App.chatCommands = {};

/**
 *
 * @type {Channel}
 */
var CHANNEL         = KnuddelsServer.getChannel();
var OWNER           = KnuddelsServer.getChannel().getChannelConfiguration().getChannelRights().getChannelOwners()[0];
var BOT             = KnuddelsServer.getDefaultBotUser();
var SERVER          = KnuddelsServer.getChatServerInfo().getServerId();
var IS_ROOT         = KnuddelsServer.getAppAccess().getOwnInstance().isRootInstance();
var APP_NAME        = 'UserApp.Cloud';
var VERSION         = (new Date()).toGermanString();


function requireClass(_class, loadDir) {
    if (typeof _class === 'undefined') {
        execute(loadDir);
    }
}

FileSystem.execute('modules/_CORE_/ModuleManager.js');
if(typeof ModuleManager.self === 'undefined') {
    ModuleManager.self = new ModuleManager();
}


ModuleManager.self.refreshHooks();




