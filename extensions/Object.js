/**
 * Vererbt Child die Klasse von Parent
 * @param child
 * @param parent
 */
Object.extends = function(child, parent) {
    child.prototype = Object.create(parent.prototype);
    child.prototype.super = function () {
        parent.call(this);
    };
    child.prototype.constructor = child;
};