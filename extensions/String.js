String.prototype.formater = function formater(obj) {
    var str = this.toString();
    obj = obj || {};



    for(let key in String.prototype.formater._registeredReplacements) {
        if(typeof obj[key] === 'undefined') {
            obj[key] = String.prototype.formater._registeredReplacements[key];
        }
    }


    var array = [];
    for(let key in obj) {
        array.push({key: key, value: obj[key]});
    }

    //verhindere dass $NAME vor $NAMEN ersetetzt wird - WICHTIG!!!!!
    array.sort(function(a,b){
        return b.key.length - a.key.length; // nach länge abwärts sortieren
    });

    for(let i in array) {
        var key = array[i].key;
        var value = array[i].value;

        var ind = -1;
        var find = '$'+key;
        while((ind = str.indexOf(find)) >= 0) {
            var newValue = value;
            if(typeof value === 'function') {
                newValue = value(find, ind, str);
            }
            str = str.substring(0, ind) + newValue + str.substr(ind+find.length);
        }
    }

    return str;
};

/**
 * Liefert die Anzahl der Vokale im String
 * @return {number}
 */
String.prototype.getVowelsCount = function() {
    let m = this.match(/[aeiouüöä]/gi);
    return m === null ? 0 : m.length;
};

/**
 * Liefert die Anzahl der unterschiedlichen Chars im String
 * @return {Number}
 */
String.prototype.getUniqueCharCount = function() {
    let tmp = {};
    for(let i  = 0; i < this.length; i++) {
        tmp[this[i]] = true;
    }
    return Object.keys(tmp).length;
};


/**
 * Prüft ob der String einer im Array vorkommen String beinhaltet
 * @param {String[]} arr
 * @return {boolean}
 */
String.prototype.hasKeywords = function(arr, ignoreCase) {
    ignoreCase = typeof ignoreCase === "undefined" ? false : ignoreCase;
    var check = this.toString();
    if(ignoreCase) {
        check = check.toLowerCase();
    }
    for(var i = 0; i < arr.length; i++) {
        var needle = arr[i];
        if(ignoreCase) {
            needle = needle.toLowerCase();
        }
        if(check.contains(needle)) return true;
    }
    return false;
};


/**
 * Ersetzt Links ohne Linktext mi
 * @return {String}
 */

String.prototype.fixDeadLinks = function() {
    return this.replace(/°>(http|\/)(?![^|<]*\.(?:gif|jpe?g|mp|png)<)([^|<]+)<°/gi,"°>$1$2|$1$2<°");
};



/**
 * Teilt den String in zufälligen Chunks auf
 * @param {Number} count
 * @param {Boolean} splitSpaces
 * @return {String[]}
 */
String.prototype.splitRandomChunks = function(count, splitSpaces) {
    let checkLengthWithoutSpaces = this.replaceAll(" ","");
    if(checkLengthWithoutSpaces.length <= count) {
        return checkLengthWithoutSpaces.split(""); //pro chunk ein char
    }
    if(typeof splitSpaces === "undefined") {
        splitSpaces = true;
    }
    let arr = [this];
    //first split for space
    if(splitSpaces) {
        arr = this.split(" ");
    }
    for(let i = arr.length; i < count; i++) {
        arr = RandomOperations.shuffleObjects(arr);
        for(let j = 0; j < arr.length; j++) {
            if(arr[j].length == 1) continue;
            let randomIndex = (arr[j].length==2?1:RandomOperations.nextInt(1, arr[j].length));
            let before = arr[j].substr(0,randomIndex);
            let after  = arr[j].substr(randomIndex);

            arr[j] = before;
            arr.splice(randomIndex,0,after);
            break;
        }
    }
    return arr;
};



String.prototype.removeComments = function() {

    var SLASH = '/';
    var BACK_SLASH = '\\';
    var STAR = '*';
    var DOUBLE_QUOTE = '"';
    var SINGLE_QUOTE = "'";
    var NEW_LINE = '\n';
    var CARRIAGE_RETURN = '\r';

    var string = this;
    var length = string.length;
    var position = 0;
    var output = [];

    function getCurrentCharacter () {
        return string.charAt(position);
    }

    function getPreviousCharacter () {
        return string.charAt(position - 1);
    }

    function getNextCharacter () {
        return string.charAt(position + 1);
    }

    function add () {
        output.push(getCurrentCharacter());
    }

    function next () {
        position++;
    }

    function atEnd () {
        return position >= length;
    }

    function isEscaping () {
        if (getPreviousCharacter() == BACK_SLASH) {
            var caret = position - 1;
            var escaped = true;
            while (caret-- > 0) {
                if (string.charAt(caret) != BACK_SLASH) {
                    return escaped;
                }
                escaped = !escaped;
            }
            return escaped;
        }
        return false;
    }

    function processSingleQuotedString () {
        if (getCurrentCharacter() == SINGLE_QUOTE) {
            add();
            next();
            while (!atEnd()) {
                if (getCurrentCharacter() == SINGLE_QUOTE && !isEscaping()) {
                    return;
                }
                add();
                next();
            }
        }
    }

    function processDoubleQuotedString () {
        if (getCurrentCharacter() == DOUBLE_QUOTE) {
            add();
            next();
            while (!atEnd()) {
                if (getCurrentCharacter() == DOUBLE_QUOTE && !isEscaping()) {
                    return;
                }
                add();
                next();
            }
        }
    }

    function processSingleLineComment () {
        if (getCurrentCharacter() == SLASH) {
            if (getNextCharacter() == SLASH) {
                next();
                while (!atEnd()) {
                    next();
                    if (getCurrentCharacter() == NEW_LINE || getCurrentCharacter() == CARRIAGE_RETURN) {
                        return;
                    }
                }
            }
        }
    }

    function processMultiLineComment () {
        if (getCurrentCharacter() == SLASH) {
            if (getNextCharacter() == STAR) {
                next();
                next();
                while (!atEnd()) {
                    next();
                    if (getCurrentCharacter() == STAR) {
                        if (getNextCharacter() == SLASH) {
                            next();
                            next();
                            return;
                        }
                    }
                }
            }
        }
    }

    function processRegularExpression (){
        if (getCurrentCharacter() == SLASH) {
            add();
            next();
            while (!atEnd()) {
                if (getCurrentCharacter() == SLASH && !isEscaping()) {
                    return;
                }
                add();
                next();
            }
        }
    }

    while (!atEnd()) {
        processDoubleQuotedString();
        processSingleQuotedString();
        processSingleLineComment();
        processMultiLineComment();
        processRegularExpression();
        if (!atEnd()) {
            add();
            next();
        }
    }
    return output.join('');


};