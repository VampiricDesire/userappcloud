/**
 *
 * @param {AppInstance} other
 */
AppInstance.prototype._equals = function(other) {
    var selfID       = this.getAppInfo().getAppUid();
    var otherID      = other.getAppInfo().getAppUid();

    return selfID === otherID;
};

OwnAppInstance.prototype._equals = function(other) {
    var selfID       = this.getAppInfo().getAppUid();
    var otherID      = other.getAppInfo().getAppUid();

    return selfID === otherID;
};

RootAppInstance.prototype._equals = function(other) {
    var selfID       = this.getAppInfo().getAppUid();
    var otherID      = other.getAppInfo().getAppUid();

    return selfID === otherID;
};