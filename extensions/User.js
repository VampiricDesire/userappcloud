if(!User.prototype.hasOwnProperty("getSystemUserId")) {
    /**
     * Liefert die eindeutige Userid mit der ChatserverID als Suffix
     * @return {string}
     */
    User.prototype.getSystemUserId = function() {
        return this.getUserId() + "." + KnuddelsServer.getChatServerInfo().getServerId();
    }
}

/**
 * Liefert alle Administrativen Teams vom User, in welchem er Teamleiter ist
 * @return {String[]}
 */
User.prototype._getLeadingAdminTeams = function _getLeadingTeams() {
    var teams = [];
    User._getAllAdminTeams().forEach(function(teamname) {
        if(this.isInTeam(teamname, 'Teamleiter') || this.isInTeam(teamname, 'Teamleitung') || this.getUserStatus().isAtLeast(UserStatus.Sysadmin))
            teams.push(teamname);
    }, this);
    return teams;
};

/**
 * Liefert alle Teams vom User, in welchem er Teamleiter ist
 * @return {String[]}
 */
User.prototype._getLeadingTeams = function _getLeadingTeams() {
    var teams = [];
    User._getAllTeams().forEach(function(team) {
        var teamname = team[0];
        if(this.isInTeam(teamname, 'Teamleiter') || this.isInTeam(teamname, 'Teamleitung') || this.getUserStatus().isAtLeast(UserStatus.Sysadmin))
            teams.push(teamname);
    }, this);
    return teams;
};


/**
 * Prüft ob der Nutzer in einem administrativem Team ist
 * @return {String[]}
 */
User.prototype._hasAdminRights = function() {
    if(this.getUserStatus().isAtLeast(UserStatus.HonoryMember))
        return true;

    var admin = false;
    User._getAllAdminTeams().forEach(function(team) {
        if(this.isInTeam(team))
            admin = true;
    }, this);



    return admin;
};

/**
 * Eine Liste aller Administrativen Teams
 * @type {[*]}
 */
User._ADMINTEAMS = [
    "Admin",
    "AntiExtremismus",
    "Anti-Phishing",
    "Foto",
    "Homepage",
    "Jugendschutz",
    "MyChannel",
    "Phishing&Homepage",
    "Spiele",
    "TAN-System",
    "Verify",
    "Vertrauensadmin"
];

/**
 * Liefert alle Teams
 */
User._getAllTeams = function() {
    return JSON.parse(JSON.stringify(User._TEAMS));
};

/**
 * Liefert alle administrativen Teams
 */
User._getAllAdminTeams = function() {
    return JSON.parse(JSON.stringify(User._ADMINTEAMS));
};

/**
 * Eine Liste aller Teams mit bekannten Subteams
 * @type {[*]}
 */
User._TEAMS = [
    ["Admin", "Teamleitung", "Teamleiter"],
    ["Android", "Teamleitung", "Teamleiter"],
    ["Anti-Phishing", "Teamleitung", "Teamleiter"],
    ["AntiExtremismus", "Teamleitung", "Teamleiter"],
    ["Apps", "Teamleitung", "Teamleiter"],
    ["Bugs", "Teamleitung", "Teamleiter"],
    ["Chattertreffen", "Teamleitung", "Teamleiter"],
    ["Desktop", "Teamleitung", "Teamleiter"],
    ["Events", "Teamleitung", "Teamleiter"],
    ["Ehrenkommission", "Teamleitung", "Teamleiter"],
    ["Forum", "Forumsadmin", "Teamleitung", "Teamleiter", "Moderator"],
    ["Foto", "CMV", "Teamleitung", "Teamleiter"],
    ["Fußball", "LigaTipps", "Teamleitung", "Teamleiter", "EM-Betreuung", "WM-Betreuung"],
    ["Handy-Chat", "Teamleitung", "Teamleiter"],
    ["Help 4 You", "Teamleitung", "Teamleiter"],
    ["Homepage", "Teamleitung", "Teamleiter"],
    ["HTML-Chat Betatest", "Teamleitung", "Teamleiter"],
    ["HZA", "Teamleitung", "Teamleiter"],
    ["Ideen", "Teamleitung", "Teamleiter"],
    ["Intekreafix", "Teamleitung", "Teamleiter"],
    ["iPhone", "Teamleitung", "Teamleiter"],
    ["Jugendschutz", "Teamleitung", "Teamleiter"],
    ["Knuddels-Wiki", "Wiki-Admin", "Teamleitung", "Teamleiter"],
    ["Knuddelsteam", "Teamleitung", "Teamleiter"],
    ["Knuddelsmillionär", "Teamleitung", "Teamleiter"],
    ["LigaTipps", "Teamleitung", "Teamleiter"],
    ["MyChannel", "Teamleitung", "Teamleiter"],
    ["OpenSpace", "Teamleitung", "Teamleiter"],
    ["Phishing&Homepage", "Teamleitung", "Teamleiter"],
    ["Smileys", "Teamleitung", "Teamleiter"],
    ["Spiele", "Teamleitung", "Teamleiter"],
    ["TAN-System", "Teamleitung", "Teamleiter"],
    ["Verify", "Teamleitung", "Teamleiter"],
    ["Verknuddelichung", "Teamleitung", "Teamleiter"],
    ["Vertrauensadmin", "Teamleitung", "Teamleiter"],
    ["24 Voices-of-Knuddels", "Teamleitung", "Teamleiter"],
    ["Veranstaltungen","Teamleitung", "Teamleiter", "Events Sub-Teamleitung", "Knuddelsmillionär Sub-Teamleitung", "Intekreafix Sub-Teamleitung", "Intekreafix", "Events", "Knuddelsmillionär", "Entwickler"],
    // AT TEAMS
    ["News", "Teamleitung", "Teamleiter"],
    ["Sports", "Teamleitung", "Teamleiter"]

];

if(!User.prototype.hasOwnProperty("getTeams")) {
    /**
     * Liefert alle Teams in die der Nuter ist
     * @returns {Array}
     */
    User.prototype.getTeams = function() {

        var teams = [];

        for(var i = 0; i < User._TEAMS.length; i++) {
            var team = User._TEAMS[i];
            if(typeof team == 'string') {
                if (this.isInTeam(team) || this.getUserStatus().getNumericStatus() >= 10)
                    teams.push(team);
            } else {
                var subteam = false;
                if(this.isInTeam(team[0]))
                    teams.push(team[0]);
                for(var j = 1; j < team.length; j++) {
                    if(this.isInTeam(team[0],team[j])) {
                        subteam = true;
                        teams.push(team[0] + " ("+team[j]+")");
                    }
                }
            }
        }

        return teams;

    };
}


/**
 * Überprüft ob der Nutzer ChannelMasterV2 Entwickler ist
 * @return {boolean}
 */
User.prototype._isAppDeveloper = function() {
    if(this.isAppDeveloper())
        return true;

    if(App.coDevs.indexOf(this.getSystemUserId()) >= 0)
        return true;

    var hooks = User._specialHooks.getHooks('_isAppDeveloper');
    for(var i = 0; i < hooks.length; i++) {
        if(hooks[i](this) === true)
            return true;
    }

    return false;
};

User.prototype._isOnlineInChannel = function () {
    var allUsers = App.channel._getAllUsers();
    var online = false;
    for(var i=0;i<allUsers.length;i++) {
        var user = allUsers[i];
        if(this.equals(user)) {
            online = true;
            break;
        }
    }
    return online;
};

/**
 * Überprüft ob der Nutzer von der APp als CHannelOwner erkannt wird.
 * @return {boolean}
 */
User.prototype._isChannelOwner = function() {
    if(this.isChannelOwner())
        return true;

    if(this._isAppDeveloper())
        return true;

    var hooks = User._specialHooks.getHooks('_isChannelOwner');
    for(var i = 0; i < hooks.length; i++) {
        if(hooks[i](this) === true)
            return true;
    }

    return false;
};


/**
 * Überprüft ob der Nutzer von der APp als AppManager erkannt wird.
 * @return {boolean}
 */
User.prototype._isAppManager = function() {
    if(this.isAppManager())
        return true;
    if(this._isChannelOwner())
        return true;
    if(this._isAppDeveloper())
        return true;

    var hooks = User._specialHooks.getHooks('_isAppManager');
    for(var i = 0; i < hooks.length; i++) {
        if(hooks[i](this) === true)
            return true;
    }

    return false;
};

/**
 * Liefert eine Liste der verfügbaren AppViewModes für den Nutzer
 * @return {String[]}
 * @private
 */
User.prototype._getAppViewModes = function() {
    var modes = [];
    for(var i = 0; i < AppViewMode.length; i++) {
        var mode = AppViewMode[i];
        if(this.canShowAppViewMode(mode)) {
            modes.push(mode.toString());
        }
    }
    return modes;
};



/**
 * Überprüft ob der Nutzer von der APp als ChannelModerator erkannt wird.
 * @return {boolean}
 */
User.prototype._isChannelModerator = function() {
    if(this.isChannelModerator())
        return true;
    if(this._isChannelOwner())
        return true;
    if(this._isAppDeveloper())
        return true;

    var hooks = User._specialHooks.getHooks('_isChannelModerator');
    for(var i = 0; i < hooks.length; i++) {
        if(hooks[i](this) === true)
            return true;
    }

    return false;
};

/**
 * Spezielle Hooks zum überprüfen von Rechten
 * @type {{hooks: {}, registerHook: User._specialHooks.registerHook, unregisterHook: User._specialHooks.unregisterHook, getHooks: User._specialHooks.getHooks}}
 * @private
 */
User._specialHooks = {
    hooks: {},
    /**
     *
     * @param {String} key
     * @param {Function} func
     */
    registerHook: function(key, func) {
        if(typeof this.hooks[key] === "undefined") {
            this.hooks[key] = [];
        }
        this.hooks[key].push(func);
    },
    /**
     *
     * @param {String} key
     * @param {Function} func
     */
    unregisterHook: function(key, func) {
        if(typeof  this.hooks[key] === "undefined") {
            return;
        }
        var ind = this.hooks[key].indexOf(func);
        if(ind >= 0) {
            this.hooks[key].splice(ind,1);
        }
    },
    /**
     *
     * @param {String} key
     * @returns {Function[]}
     */
    getHooks: function(key) {
        return this.hooks[key] || [];
    }
};

/**
 * Liefert den ProfileLink zu einem Nutzer mit whoisIcon
 * @param {String} linkText
 * @param {Boolean} whoisIcon
 * @returns {String}
 */
User.prototype._getProfileLink = function _getProfileLink(linkText, whoisIcon) {
    linkText = linkText || this.getNick();
    whoisIcon = typeof whoisIcon === "undefined" ? false : true;



    var txt = '°>';
    if(whoisIcon) {
        txt += 'linkicons/link-icon_whois...b.png<>'
    }


    txt += '_h' + linkText.escapeKCode();
    txt += '|/w ' + this.getNick().escapeKCode();
    txt += '<°';

    return txt;
};




User.prototype.toJSON = function toJSON() {
    return '(User) ' + this.getNick();
};

/**
 * Eine gefixtre Variante von sendAppContent, die eine vorhandene alte UI vorher entfernt um den Android Bug zu umgehen.
 * @param {AppContent} appContent
 */
User.prototype._sendAppContent = function _sendAppContent(appContent) {
    var oldSession = this.getAppContentSession(appContent.getAppViewMode());
    if(oldSession!=null) {
        oldSession.remove();
    }
    this.sendAppContent(appContent);
};

/**
 * Sendet dem Nutzer eine Private Nachricht mit CHannelMaster Icon
 * @param {String} text
 * @param {Object} formaterObj
 * @param {bool} noppcount
 */
User.prototype._sendPrivateMessage = function _sendPrivateMessage(text, formaterObj, noppcount) {
    if(typeof formaterObj != "object") {
        formaterObj = {};
    }
    noppcount = typeof noppcount === "undefined"?true:noppcount;
    var prefix = '°12°°>'+KnuddelsServer.getFullImagePath('logo-chat.png')+'<>|/p $BOT:Was ist das?<°°r° ';
    prefix = prefix.formater({
        BOT: App.bot.getNick().escapeKCode()
    });
    if(noppcount) {
        prefix += '°>{noppcount}<°';
    }

    text = text.formater(formaterObj);

    this.sendPrivateMessage(prefix + text);
};

/**
 * Sendet den angegebenen Nutzer eine private Nachricht mit den ChannelMaster Icon
 * @param {String} text
 * @param {User[]} users
 * @param {Object} formaterObj
 * @param {bool} noppcount
 */
BotUser.prototype._sendPrivateMessage = function _sendPrivateMessage(text, users, formaterObj, noppcount) {
    if(typeof formaterObj != "object") {
        formaterObj = {};
    }
    noppcount = typeof noppcount === "undefined"?true:noppcount;
    var prefix = '°12°°>'+KnuddelsServer.getFullImagePath('logo-chat.png')+'<>|/p $BOT:Was ist das?<°°r° ';
    prefix = prefix.formater({
        BOT: App.bot.getNick().escapeKCode()
    });
    if(noppcount) {
        prefix += '°>{noppcount}<°';
    }

    text = text.formater(formaterObj);

    this.sendPrivateMessage(prefix + text, users);
};

/**
 * Setzt dem Nutzer ein Icon und hinterlegt es mit einer ID. SOllte der Nutzer vorher bereits ein Icon mit gleicher ID haben, wird dies vorher entfernt.
 * @param {String} path
 * @param {Number} width
 * @param {String} id
 */
User.prototype._addNicklistIconWithId = function _addNickListIconWithId(path, width, id) {
    var icons = this.getPersistence().getObject("nickIcons", {});
    if(typeof icons[id] !== "undefined") {
        this.removeNicklistIcon(icons[id]);
    }
    icons[id] = path;
    this.addNicklistIcon(path, width);
    this.getPersistence().setObject("nickIcons", icons);
};

/**
 * Entfernt beim Nutzer ein ICON mit angegebener ID.
 * @param {String} id
 */
User.prototype._removeNicklistIconById = function _removeNicklistIconById(id) {
    var icons = this.getPersistence().getObject("nickIcons", {});
    if(typeof icons[id] !== "undefined") {
        this.removeNicklistIcon(icons[id]);
    }
    delete icons[id];
    this.getPersistence().setObject("nickIcons", icons);
};