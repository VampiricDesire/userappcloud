/**
 * Liefert alle User (mit UserType) im Channel und Tochterchannel
 * @property {function}
 * @name Channel#_getAllUsers
 * @param {UserType} [usertype=UserType#Human]
 * @return {User[]}
 */
Channel.prototype._getAllUsers = function (usertype) {
    usertype = usertype || UserType.Human;
    let userArr = [];
    let cache = {};
    let ownInstance = KnuddelsServer.getAppAccess().getOwnInstance();
    let allInstances = ownInstance.getAllInstances(true);

    allInstances.forEach(function (instance) {
        let users = ownInstance.getOnlineUsers(instance, usertype);
        users.forEach(function (user) {
            if (typeof cache[user.getUserId()] === 'undefined') {
                userArr.push(user);
                cache[user.getUserId()] = true;
            }
        });
    });

    return userArr;

};


/**
 * Liefert eine Map aller registrierten Chatbefehle in diesem Channel
 * @property {function}
 * @name Channel#_getRegisteredChatCommandNames
 * @param {Boolean} [includeSelf=false]
 * @return {Object}
 */
Channel.prototype._getRegisteredChatCommandNames = function (includeSelf) {


    includeSelf = typeof includeSelf === 'undefined' ? false : includeSelf;
    let instances = KnuddelsServer.getAppAccess().getAllRunningAppsInChannel(includeSelf);

    let commands = {};

    instances.forEach(function (instance) {
        let cmds = instance.getRegisteredChatCommandNames();
        cmds.forEach(function (cmd) {
            commands[cmd.toLowerCase()] = instance;
        })
    });
    return commands;

}
;


Channel.prototype.toJSON = function () {
    return '(Channel) ' + this.getChannelName();
};