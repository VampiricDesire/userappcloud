AppContentSession.prototype._sendPartialData = function (key, data) {
    if(typeof data !== "string") {
        data = JSON.stringify(data);
    }
    var arr = data.match(/.{1,7000}/g);
    for(var  i = 0; i < arr.length; i++) {
        var newData = {
            key: key,
            data: arr[i],
            page: i,
            length: arr.length
        };
        this.sendEvent("PartialData", newData);
    }
};