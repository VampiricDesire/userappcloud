/**
 * Liefert die absolute URL vom KnuddelsCDN
 * @param {string} filePath
 * @returns {string}
 */
KnuddelsServer._getImagePathFromKCDN = function(filePath) {
    filePath = filePath.trim();
    while(filePath[0]=='/') {
        filePath = filePath.substr(1);
    }
    filePath = filePath.replaceAll('pics/','');
    return 'https://cdnc.knuddels.de/pics/'+filePath;
};

if(!(typeof KnuddelsServer.getUserByNickname === 'function')) {
    /**
     * Gibt das Userobject von Nickname wieder. Ist null, wenn Nutzer nicht existiert oder nicht zugegriffen werden darf
     * @param {string} nickname
     * @returns {User}
     */
    KnuddelsServer.getUserByNickname = function (nickname) {
        nickname = nickname.trim();
        if (!KnuddelsServer.userExists(nickname)) {
            return null;
        }
        var userid = KnuddelsServer.getUserId(nickname);
        if (!KnuddelsServer.canAccessUser(userid)) {
            return null;
        }

        return KnuddelsServer.getUser(userid);
    };
}


/**
 * Liefert alle Nutzer (inkl. James) im Channel + Tochterchannel
 * @return {User[]}
 */
KnuddelsServer._getAllUsers = function() {
    return KnuddelsServer.getChannel()._getAllUsers();
};

if(typeof KnuddelsServer.isTestSystem === 'undefined') {
    /**
     * alias für KnuddelsServer.getChatServerInfo().isTestSystem();
     * @return {Boolean}
     */
    KnuddelsServer.isTestSystem = function isTestSystem() {
        return KnuddelsServer.getChatServerInfo().isTestSystem();
    }
}