if(!Number.prototype.hasOwnProperty("number_format")) {
    /**
     * Formatiert eine Zahk mit tausender Seperatoren, Kommazeichen sowie Anzahl der Stellen nach dem Komma
     * @param {Number} decimals Anzahl der Stellen nach dem Komma
     * @param {String} dec_point Dezimalzeichen (def: .)
     * @param {String} thousands_sep Kommazeichen (def: ,)
     * @return {string}
     */
    Number.prototype.number_format = function(decimals, dec_point, thousands_sep) {

        var n = !isFinite(+this) ? 0 : +this,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            toFixedFix = function (n, prec) {
                // Fix for IE parseFloat(0.55).toFixed(0) = 0;
                var k = Math.pow(10, prec);
                return Math.round(n * k) / k;
            },
            s = (prec ? toFixedFix(n, prec) : Math.round(n)).toString().split('.');
        if (s[0].length > 3) {
            s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
        }
        if ((s[1] || '').length < prec) {
            s[1] = s[1] || '';
            s[1] += new Array(prec - s[1].length + 1).join('0');
        }
        return s.join(dec);
    };
}

/**
 * Wandelt die Zahl in ein KnuddelAmount um.
 * @return {KnuddelAmount}
 */
Number.prototype._toKnuddelAmount = function () {
    var cents = ~~(this*100);
    return KnuddelAmount.fromCents(cents);
};

/**
 * Wandelt die Zahl ordentlich gerundet für KnuddelAmount um.
 * @return {Number}
 */
Number.prototype._toKnuddelAmountNumber = function() {
    return this._toKnuddelAmount().asNumber();
};
